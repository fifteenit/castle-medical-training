<?php

/**

 * Header Template

 * @file           header.php

 * @package        Castle Medical Training

 * @filesource     wp-content/themes/castle/header.php

 * @since          Castle Medical Training 1.0

*/

?>

<!doctype html>

<!--[if lt IE 7 ]> <html class="no-js ie6" <?php language_attributes(); ?>> <![endif]-->

<!--[if IE 7 ]>    <html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->

<!--[if IE 8 ]>    <html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->

<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>


	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />



	

	<link href="https://fonts.googleapis.com/css?family=PT+Sans:700|Roboto:400,700" rel="stylesheet"> 

</head>

<body>

	<div class="sitewrapper">

    	<?php if (get_field('contact_number','option') || get_field('contact_email','option') || get_field('facebook','option') || get_field('twitter','option') || get_field('snapchat','option') || get_field('instagram','option') || get_field('google_plus','option') ) { ?>

            <div class="topbar wrapper">

                <div class="content">

                    <?php if (get_field('contact_number','option')) { ?>

                        <?php $phone = get_field('contact_number','option'); ?>

                        <?php $countryCode = '44'; ?>

                        <?php $tel = preg_replace("/[^0-9]/", "", $phone); ?>

                        <?php $tel = preg_replace('/^0?/', '+'.$countryCode, $tel); ?>

                        <a class="phone" href="tel:<?php echo $tel ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php the_field('contact_number','option') ?></a>

                    <?php } ?>

                    <?php if (get_field('contact_email','option')) { ?>

                        <a class="email" href="mailto:<?php the_field('contact_email','option') ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <?php the_field('contact_email','option') ?></a>

                    <?php } ?>

					<?php if (get_field('facebook','option')) { ?>

                        <a class="facebook" href="<?php the_field('facebook','option') ?>"target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>

                    <?php } ?>
                      <?php if (get_field('whatapp','option')) { ?>

                        <a class="whatapp" href="<?php the_field('whatapp','option') ?>"target="_blank"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>

                    <?php } ?>

                    <?php if (get_field('twitter','option')) { ?>

                        <a class="twitter" href="<?php the_field('twitter','option') ?>"target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a>

                    <?php } ?>

                    <?php if (get_field('google_plus','option')) { ?>

                        <a class="googleplus" href="<?php the_field('google_plus','option') ?>"target="_blank"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a>

                    <?php } ?>

                    <?php if (get_field('snapchat','option')) { ?>

                        <a class="snapchat" href="<?php the_field('snapchat','option') ?>"target="_blank"><i class="fab fa-snapchat-ghost" aria-hidden="true"></i></a>

                    <?php } ?>

                    <?php if (get_field('instagram','option')) { ?>

                        <a class="instagram" href="<?php the_field('instagram','option') ?>"target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a>

                    <?php } ?>

                </div>

            </div>

       	<?php } ?>

    	<div class="header wrapper">

        	<div class="content">

            	<div class="flexwrapper alignmiddle spacebetween">

                	<div class="site-logo">

						<?php $custom_logo_id = get_theme_mod( 'custom_logo' );

                                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

                        ?>

                        <?php if ( $custom_logo_id ) { ?>

                            <div class="logo">

                                <a href="<?php echo get_home_url(); ?>" rel="home"><img src="<?php echo $image[0]; ?>" alt="<?php echo get_bloginfo('name');?> - <?php echo get_bloginfo('description'); ?>"/></a>

                            </div>

                        <?php } else { ?>

                            <h2 class="site-title"><a href="<?php echo get_home_url(); ?>" rel="home"><?php echo get_bloginfo('name');?></a></h2><br/>

                            <span class="site-description"><?php echo get_bloginfo('description'); ?></span>

                        <?php } ?>

    				</div>

                    <nav class="mainnavmenu">

                        <?php wp_nav_menu( array('theme_location' => 'main'));?>

                    </nav>

                </div>

            </div>

        </div>

    