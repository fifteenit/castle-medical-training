<?php

/**

 * footer Template

 * @file           footer.php

 * @package        Castle Medical Training

 * @filesource     wp-content/themes/castle/footer.php

 * @since          Castle Medical Training 1.0

*/

?>

		<div class="footer wrapper">

			<div class="content">

				<div class="flexwrapper spacebetween">

					<div class="footermenu footerblock">

                        <h3 class="footer-title">More Information</h3>

                    	<nav class="footernavmenu">

                    		<?php wp_nav_menu( array('theme_location' => 'footer'));?>

                       	</nav>

                	</div>

                	<div class="footerblock">

                        <h3 class="footer-title">Address</h3>

                        	<div class="companyinfo">

								<?php if (get_field('copyright_name','options')) { ?>

                                    <p><a href="<?php echo get_home_url(); ?>" rel="home"><?php the_field('copyright_name','options');?></a></p>

                                <?php } ?>

                                <?php if (get_field('company_number','option') ) { ?>

                                    <p><strong>Company No:</strong> <?php the_field('company_number','option');?><br/></p>

                                <?php } ?>

                                <?php if (get_field('company_address','option') ) { ?>

                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i><span class="value"><?php the_field('company_address','option');?></span></p>

                                <?php } ?>

                                    <?php if (get_field('contact_number','option')) { ?>

                                    <?php $phone = get_field('contact_number','option'); ?>

                                    <?php $countryCode = '44'; ?>

                                    <?php $tel = preg_replace("/[^0-9]/", "", $phone); ?>

                                    <?php $tel = preg_replace('/^0?/', '+'.$countryCode, $tel); ?>

                                    <p><a class="phone" href="tel:<?php echo $tel ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span class="value"><?php the_field('contact_number','option') ?></span></a></p>

                                <?php } ?>

                                <?php if (get_field('contact_email','option')) { ?>

                                    <p><a class="email" href="mailto:<?php the_field('contact_email','option') ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <span class="value"><?php the_field('contact_email','option') ?></span></a></p>

                                <?php } ?>

                          	</div>

                	</div>

                        <?php if (get_field('facebook','option') || get_field('twitter','option') || get_field('instagram','option') || get_field('snapchat','option') || get_field('google_plus','option')) { ?>

                        	<div class="social footerblock">

                            	<h3 class="footer-title">Social Media</h3>

							<?php if (get_field('facebook','option')) { ?>

                                <a class="facebook" target="_blank" href="<?php the_field('facebook','option') ?>"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>

                            <?php } ?>

							<?php if (get_field('twitter','option')) { ?>

                                <a class="twitter" target="_blank" href="<?php the_field('twitter','option') ?>"><i class="fab fa-twitter" aria-hidden="true"></i></a>

                            <?php } ?>

							<?php if (get_field('google_plus','option')) { ?>

                                <a class="googleplus" target="_blank" href="<?php the_field('google_plus','option') ?>"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a>

                            <?php } ?>

							<?php if (get_field('snapchat','option')) { ?>

                                <a class="snapchat" target="_blank" href="<?php the_field('snapchat','option') ?>"><i class="fab fa-snapchat-ghost" aria-hidden="true"></i></a>

                            <?php } ?>

							<?php if (get_field('instagram','option')) { ?>

                                <a class="instagram" target="_blank" href="<?php the_field('instagram','option') ?>"><i class="fab fa-instagram" aria-hidden="true"></i></a>

                            <?php } ?>
                            
                            <?php if (get_field('whatapp','option')) { ?>
                            
                            	<a class="whatapp" target="_blank" href="<?php the_field('whatapp','option') ?>"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
                                
                                <?php } ?>

                            </div>

                      	<?php } ?>

                </div>

                <div class="copyright">

						<p><?php echo wpb_copyright(); ?> - <a href="<?php echo get_home_url(); ?>" rel="home">Castle Medical Training</a></p>
                        <p> Development by <a href="https://fifteenit.co.uk/"target="_blank"> Fifteen IT Ltd </a> </p>

                </div>

			</div>

      	</div>

		<?php wp_footer(); ?>

<!--                <script>

						jQuery(document).ready(function($) { 

							$('.pagepiling').pagepiling({

								verticalCentered: true,

								navigation: { 

									'bulletsColor': '#fff',

									'position': 'bottom',

									//'tooltips': ['Top', <?php //the_field('tooltip_titles', $post_id);?>, 'Footer']

								},

								//anchors: ['top', <?php //the_field('anchors', $post_id);?>, 'footer'],

								normalScrollElements: '.overlay',

								touchSensitivity: 20,

								

/*								afterRender: function () {

									setInterval(function () {

										$.fn.pagepiling.moveSectionDown();

									}, 5000);

								}*/

								

							});

						}); 

					

                </script>

-->	</div>

</body>

</html>