	<?php                            
        $border = get_sub_field('border_location'); ?>
            <div class="section section-background image-only 
                <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
                
				<?php if (get_sub_field('video')) { ?>
                <?php $iframe = get_sub_field('video');
					preg_match('/src="(.+?)"/', $iframe, $matches);
					$src = $matches[1];
					$params = array(
						'controls'    => 0,
						'hd'        => 1,
						'autohide'    => 1
					); 
					$new_src = add_query_arg($params, $src);
					
					$iframe = str_replace($src, $new_src, $iframe);
					
					
					// add extra attributes to iframe html
					$attributes = 'frameborder="0"';
					
					$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
					?>
                
                    <div class="embed-container">
                        <?php  the_sub_field('video'); ?>
                        <?php //echo $iframe; ?>
                    </div>
                <?php } ?>
            </div>
    <?php wp_reset_postdata();?>
