	<?php 
        $setColor =  get_sub_field('background_colour', $post->ID);
        $color = $setColor;
        $rgb = hex2rgba($color);
        $rgba = hex2rgba($color, 1);
        $border = get_sub_field('border_location');
    ?>
        <?php if ( $rgba ) { ?>
            <div class="section colourbg pp-scrollable gallery <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
        <?php } else { ?>
            <div class="section pp-scrollable gallery <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
            <?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
        <?php } ?>
        	<div class="pp-tableCell" style="height:100%">
                <div class="content">
                        <?php if (get_sub_field('section _title')) { ?>
                            <h2 class="section-title"<?php if (get_sub_field('text_colour')) { ?>style="color:<?php the_sub_field('text_colour');?>;"<?php }?>><?php the_sub_field('section _title');?></h2>
                        <?php } ?>
                        <?php $images = get_sub_field('gallery_images'); ?>
                        <?php if( $images ){ ?>
                            <div class="fancybox-wrapper overlay">
                                <ul class="fancybox-slides">
                                    <?php foreach( $images as $image ) { ?>
                                        <?php $img_id = $image['ID']; ?>
                                        <?php $gimage = wp_get_attachment_image_src( $img_id, 'medium' );?> 
                                        <?php $fimage = wp_get_attachment_image_src( $img_id, 'full' );?>
                                        <?php $caption = $image['caption'] ?> 
                                        <?php //print_r ($image) ; ?>
                                            <div class="fancybox-wrapper">
                                                <a data-fancybox="gallery" data-caption="<?php echo $caption; ?>" href="<?php echo $fimage[0]; ?>">
                                                        <img src="<?php echo $gimage[0]; ?>" alt="$gimage[]"/>
                                                </a>
                                                <span class="caption"><?php echo $caption;?></span>
                                            </div>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php if (get_sub_field('link_to_gallery_page')) { ?>
                                <a class="button" href="<?php the_sub_field('link_to_gallery_page');?>"><?php if (get_sub_field('link_to_button_text')) { ?><?php the_sub_field('link_to_button_text');?><?php } else { ?>See More<?php } ?></a>
                            <?php } ?>
                    <?php } ?>
                </div>
           	</div>
        </div>
