	<?php
        $setColor =  get_sub_field('overlay_colour', $post->ID);
        $alphanum = get_sub_field('overlay_opacity', $post->ID);
        $color = $setColor;
        $alpha = $alphanum/100;
        $rgb = hex2rgba($color);
        $rgba = hex2rgba($color, $alpha);
    ?>
    <?php 
        $bgimage = get_sub_field('full_image');
        //$size = 'banner'; // (thumbnail, medium, large, full or custom size)
        $border = get_sub_field('border_location');
        
        if( $bgimage ) { ?>
            <div class="section section-background <?php if (get_sub_field('background_position')){ echo ' '. the_sub_field('background_position'); } ?><?php if (get_sub_field('background_behaviour')){ echo ' '. the_sub_field('background_behaviour'); }?>
			<?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
			<?php if (get_sub_field('full_image')) { ?>
                background-image:url('<?php echo wp_get_attachment_image_url( $bgimage, 'full' );?>');
			<?php } ?>
			
			<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
        <?php } else { ?>
            <div class="section "
			<?php if (get_sub_field( 'anchor' ) ) { ?>
                data-anchor="<?php the_sub_field( 'anchor' ); ?>"
            <?php } ?>
            >
       	<?php } ?>
        	<div class="pp-tableCell" style="height:100%;
                <?php if (get_sub_field('container_vertical_position')) { ?>vertical-align:
                	<?php if (get_sub_field('container_vertical_position') == "Top") {?>
                    	top;
                        padding-top:48pt;
                    <?php } else if (get_sub_field('container_vertical_position') == "Middle") {?>
                    	middle;
                    <?php } else if (get_sub_field('container_vertical_position') == "Bottom") {?>
                    	bottom;
                        padding-bottom:96pt;
                    <?php } ?>
                <?php } ?>
            ">
            	<div class="content" style="
                <?php if (get_sub_field('max_container_width')) { ?>max-width:<?php the_sub_field('max_container_width');?>%;<?php } ?>
                <?php if (get_sub_field('container_position')) { ?>margin:
                
					<?php if (get_sub_field('container_position') == "Left") {?>
                		0 auto 0 0;
                        padding-left:24pt;
					<?php } else if (get_sub_field('container_position') == "Center") {?>
                    	0 auto;
					<?php } else if (get_sub_field('container_position') == "Right") {?>
                    	0 0 0 auto;
                        padding-right:24pt;
                    <?php } ?>
                <?php } ?>
                ">
                	<?php echo $size; ?>
					<?php if ( $rgba ) { ?>
                        <?php if ( get_sub_field('text_colour' ) ) { ?>
                            <div class="overlay" style="background:<?php echo $rgba ?>;color:<?php the_sub_field('text_colour');?>">
                        <?php } else { ?>
                            <div class="overlay" style="background:<?php echo $rgba ?>;">
                        <?php } ?>
                    <?php } ?>
							<?php if (get_sub_field('section_title')) { ?>
                                <h2 class="section-title"><?php the_sub_field('section_title');?></h2>
                            <?php } ?>
                            <?php if (get_sub_field('section_content')) { ?>
                                <?php the_sub_field('section_content');?>
                            <?php } ?>
                    <?php if ( $rgba ) { ?>
                        </div>
                    <?php } ?>
              	</div>
          	</div>
    <?php
        if( $bgimage ) { ?>
            </div>
    <?php } ?>
