	<?php 
        $setColor =  get_sub_field('background_colour', $post->ID);
        $color = $setColor;
        $rgb = hex2rgba($color);
        $rgba = hex2rgba($color, 1);
        $border = get_sub_field('border_location');
    ?>
    <?php if (get_sub_field( 'anchor' ) ) { ?>
        <a id="<?php the_sub_field( 'anchor' ); ?>"></a>
    <?php } ?>
        <?php if ( $rgba ) { ?>
            <?php if ( get_sub_field('text_colour' ) ) { ?>
                <div class="colourbg nopadding<?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;color:<?php the_sub_field('text_colour');?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"/>
            <?php } else { ?>
                <div class="colourbg <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>">
            <?php } ?>
        <?php } else { ?>
            <div class="section <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
            <?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>">
        <?php } ?>
            <div class="content">
            	<?php if (have_rows('cta')) { ?>
                	<div class="flexwrapper ctawrapper">
                    	<?php while(have_rows('cta')) { ?>
                    		<?php the_row();?>
                            	<?php if (get_sub_field('link_colour')) { ?>
                                <?php } ?>
                            	<?php if (get_sub_field('block_link')) {?>
                                    <a style=" <?php if (get_sub_field('link_colour')) { ?>color:<?php echo $rgba ?>;!important;<?php } ?> background:<?php echo $rgba ?>;" class="cta-link" href="<?php the_sub_field('block_link');?>">
                                        <div class="cta-object">
                                            <?php if (get_sub_field('block_icon')) { ?>
                                                <?php the_sub_field('block_icon');?>
                                            <?php } ?>
                                            <?php if (get_sub_field('block_text')) { ?>
                                                <?php the_sub_field('block_text');?>
                                            <?php } ?>
                                        </div>
                                  	</a>
                              	<?php } else { ?>
                                        <div class="cta-object">
                                            <?php if (get_sub_field('block_icon')) { ?>
                                                <?php the_sub_field('block_icon');?>
                                            <?php } ?>
                                            <?php if (get_sub_field('block_text')) { ?>
                                                <?php the_sub_field('block_text');?>
                                            <?php } ?>
                                        </div>                                
                                <?php } ?>
                    	<?php } ?>
                	</div>
                <?php } ?>
            </div>
        </div>
