	<?php 
        $setColor =  get_sub_field('background_colour', $post->ID);
        $color = $setColor;
        $rgb = hex2rgba($color);
        $rgba = hex2rgba($color, 1);
        $border = get_sub_field('border_location');
		$bgimage = get_sub_field('full_image');
		$overlaycolour = get_sub_field('overlay_colour', $post->ID);
        $alphanum = get_sub_field('overlay_opacity', $post->ID);
        $color1 = $overlaycolour;
        $alpha = $alphanum/100;
        $overlayrgb = hex2rgba($color1);
        $overlayrgba = hex2rgba($color1, $alpha);
    ?>
    	<?php if ($bgimage) { ?>
            <?php if ( get_sub_field('text_colour' ) ) { ?>
                <div class="section  section-background <?php if (get_sub_field('background_position')){ echo ' '. the_sub_field('background_position'); } ?><?php if (get_sub_field('background_behaviour')){ echo ' '. the_sub_field('background_behaviour'); }?> <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
                background-image:url('<?php echo wp_get_attachment_image_url( $bgimage, 'full' );?>');
                color:<?php the_sub_field('text_colour');?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
            <?php } else { ?>
                <div class="section  section-background <?php if (get_sub_field('background_position')){ echo ' '. the_sub_field('background_position'); } ?><?php if (get_sub_field('background_behaviour')){ echo ' '. the_sub_field('background_behaviour'); }?> <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
                background-image:url('<?php echo wp_get_attachment_image_url( $bgimage, 'full' );?>');
				<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
            <?php } ?>
        
        
        <?php } else if ( $rgba ) { ?>
            <?php if ( get_sub_field('text_colour' ) ) { ?>
                <div class="section  colourbg <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;color:<?php the_sub_field('text_colour');?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
                
            <?php } else { ?>
            
                <div class="section  colourbg <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
            <?php } ?>
            
            
        <?php } else { ?>
        
            <div class="section  <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
            <?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
        <?php } ?>
        	<!--<div class="pp-tableCell" style="height:100%;
                <?php if (get_sub_field('container_vertical_position')) { ?>vertical-align:
                	<?php if (get_sub_field('container_vertical_position') == "Top") {?>
                    	top;
                        padding-top:48pt;
                    <?php } else if (get_sub_field('container_vertical_position') == "Middle") {?>
                    	middle;
                    <?php } else if (get_sub_field('container_vertical_position') == "Bottom") {?>
                    	bottom;
                        padding-bottom:96pt;
                    <?php } ?>
                <?php } ?>
            ">-->
                <div class="content" style="
                <?php if (get_sub_field('max_container_width')) { ?>max-width:<?php the_sub_field('max_container_width');?>%;<?php } ?>
                <?php if (get_sub_field('container_position')) { ?>margin:
                
					<?php if (get_sub_field('container_position') == "Left") {?>
                		0 auto 0 0;
                        padding-left:24pt;
					<?php } else if (get_sub_field('container_position') == "Center") {?>
                    	0 auto;
					<?php } else if (get_sub_field('container_position') == "Right") {?>
                    	0 0 0 auto;
                        padding-right:24pt;
                    <?php } ?>
                <?php } ?>
                ">
                    <div class="overlay" <?php if(get_sub_field('overlay') == 'true'){ ?>style="background:<?php echo $overlayrgba ?>;"<?php } ?>>
                        <?php if (get_sub_field('section_title')) { ?>
                            <h2 class="section-title"><?php the_sub_field('section_title');?></h2>
                        <?php } ?>
                        <?php if (have_rows('columns')) {?>
                            <div class="columns flexwrapper">
                                <?php while (have_rows('columns')) {?>
                                    <?php the_row();?>
                                    
                                        <div class="columnwrapper">
                                            <?php if (get_sub_field('column_title')){ ?>
                                                <h3 class="column-title"><?php the_sub_field('column_title');?></h3>
                                            <?php } ?>
                                            <?php if (get_sub_field('column_content')){ ?>
                                                <div class="column-content"><?php the_sub_field('column_content');?></div>
                                            <?php } ?>
                                        </div>
                                <?php } ?>
                            </div> 
                        <?php } ?>
                    </div>
               	<!--</div>-->
            </div>
    <?php if (get_sub_field( 'anchor' ) ) { ?>
        <a id="<?php the_sub_field( 'anchor' ); ?>"></a>
    <?php } ?>
        </div>
