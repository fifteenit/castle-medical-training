	<?php 
        $setColor =  get_sub_field('background_colour', $post->ID);
        $color = $setColor;
        $rgb = hex2rgba($color);
        $rgba = hex2rgba($color, 1);
        $border = get_sub_field('border_location');
    ?>
    <?php if (get_sub_field( 'anchor' ) ) { ?>
        <a id="<?php the_sub_field( 'anchor' ); ?>"></a>
    <?php } ?>
        <?php if ( $rgba ) { ?>
            <div class="colourbg gallery-slider <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>">
        <?php } else { ?>
            <div class="section gallery-slider <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
            <?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>">
        <?php } ?>
                <?php if (get_sub_field('section_title')) { ?>
                <div class="content">
                    <h2 class="section-title"<?php if (get_sub_field('text_colour')) { ?>style="color:<?php the_sub_field('text_colour');?>;"<?php }?>><?php the_sub_field('section_title');?></h2>
                </div>
                <?php } ?>
                <?php if (have_rows('gallery_slides')){ ?>
                    <div class="flexslider">
                        <ul class="slides">
						<?php while (have_rows('gallery_slides')) { ?>
							<?php the_row();?>
							<?php $images = get_sub_field('gallery_images'); ?>
                            <?php $size = 'banner'; ?>
							<?php if( $images ){ ?>
                                    <li>
                                        <?php if (get_sub_field('slide_link')) { ?>
                                            <a href="<?php the_sub_field('slide_link');?>" rel="bookmark" target="_blank">
                                                <?php echo wp_get_attachment_image( $images, $size ); ?>
                                            </a>
                                        <?php } else { ?>
                                            <?php echo wp_get_attachment_image( $images, $size ); ?>
                                        <?php } ?>
                                    </li>
                            <?php } ?>
                 	<?php } ?>
                    </ul>
                    </div>
               	<?php } ?>
				</div>