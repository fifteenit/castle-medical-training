

<?php if (have_rows('show_details')) { ?>
	<div class="section showswrapper">
		<?php while (have_rows('show_details')) { ?>
            <?php the_row();?>
                <?php if (get_sub_field('headline_title')) { ?><h3 class="showtitle"><?php the_sub_field('headline_title');?></h3><?php } ?>
                <div class="show-entry">
                    <?php if (get_sub_field('date_of_show')) { ?><span class="showdate"><i class="fas fa-calendar"></i> <?php the_sub_field('date_of_show');?></span><br/><?php } else { ?><span class="tbc">TBC</span><br/><?php }?>
                    <?php if (get_sub_field('venue')) { ?><strong class="venue"><?php the_sub_field('venue');?></strong><br/><?php } ?>
                    <?php if (get_sub_field('number')) { ?><span class="vendorphone"><i class="fa fa-phone" aria-hidden="true"></i> <?php the_sub_field('number');?></span><br/><?php } ?>
                    <?php if (get_sub_field('vendor')) { ?><a class="vendorlink" target="_blank" href="<?php the_sub_field('vendor');?>"><i class="fas fa-globe"></i> <?php if (get_sub_field('vendor_text')) { ?><?php the_sub_field('vendor_text'); } else { ?>Tickets<?php } ?></a><br/><?php } ?>
                    <?php if (get_sub_field('headline_description')) { ?><span class="showdescription"><?php the_sub_field('headline_description');?></span><?php } ?>
                    <?php if (get_sub_field('sale_status')) { ?><strong class="salestatus"><?php the_sub_field('sale_status');?></strong><?php } ?>
                </div>
        <?php } ?>
    </div>
<?php } ?>