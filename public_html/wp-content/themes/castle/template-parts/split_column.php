	<?php 
        $setColor =  get_sub_field('background_colour', $post->ID);
        $color = $setColor;
        $rgb = hex2rgba($color);
        $rgba = hex2rgba($color, 1);
        $border = get_sub_field('border_location');
        $bgimage = get_sub_field('full_image');
        
        $setOverlayColor =  get_sub_field('overlay_colour', $post->ID);
        $alphanumover = get_sub_field('overlay_opacity', $post->ID);
        $colorover = $setOverlayColor;
        $alphaover = $alphanumover/100;
        $orgb = hex2rgba($color);
        $orgba = hex2rgba($color, $alphaover);
        
    ?>
        <?php if ( $rgba ) { ?>
            <?php if ( get_sub_field('text_colour' ) ) { ?>
                <div class="section  split 
					<?php if (get_sub_field('text_block_alignment') == 'Left' ) { ?>splitleft<?php }?>
					<?php if (get_sub_field('text_block_alignment') == 'Right' ) { ?>splitright<?php } ?>

				<?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="<?php if (get_sub_field('background_colour')){?>background-color:<?php the_sub_field('background_colour'); ?>;<?php }?><?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
            <?php } else { ?>
                <div class="section  split 
					<?php if (get_sub_field('text_block_alignment') == 'Left' ) { ?>splitleft<?php }?>
					<?php if (get_sub_field('text_block_alignment') == 'Right' ) { ?>splitright<?php } ?>

				<?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style=" <?php if (get_sub_field('background_colour')){?>background-color:<?php the_sub_field('background_colour'); ?>;<?php }?><?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
            <?php } ?>
        <?php } else { ?>
            <div class="section  <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
                <?php } ?>
				>
        <?php } ?>
        	<!--<div class="pp-tableCell" style="height:100%">-->
                <?php if (get_sub_field('text_block_alignment') == 'Left' ) { ?>
                    <div class="flexwrapper splitwrapper">
                        <div class="block splitcontent splitcontentleft">
                            <?php if ( $orgba ) { ?>
                                <div class="overlay split-overlay" style="color:<?php the_sub_field('text_colour');?>;">
                            <?php } ?>
                            	<div class="">
									<?php if (get_sub_field('section_title')) { ?>
                                        <h2 class="section-title"><?php the_sub_field('section_title');?></h2>
                                    <?php } ?>
                                    <?php if (get_sub_field('section_content')) { ?>
                                        <?php the_sub_field ('section_content');?>
                                    <?php } ?>
                                    <?php if (get_sub_field('shortcode')) { ?>
                                        <?php echo do_shortcode(get_sub_field('shortcode')); ?>
                                    <?php } ?>
                              	</div>
                            <?php if ( $orgba ) { ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="block imageright">
                            <?php $bgimage = get_sub_field('full_image'); ?>
                            <?php $size = 'full';?>
                            <?php echo wp_get_attachment_image( $bgimage, $size );?>
                        </div>
                <?php } else { ?>
                    <div class="flexwrapper splitwrapper">
                        <div class="block imageleft">
                            <?php $bgimage = get_sub_field('full_image'); ?>
                            <?php $size = 'full';?>
                            <?php echo wp_get_attachment_image( $bgimage, $size );?>
                        </div>
                        <div class="block splitcontent splitcontentright">
                            <?php if ( $orgba ) { ?>
                                <div class="overlay split-overlay" style="color:<?php the_sub_field('text_colour');?>;">
                            <?php } ?>
                            	<div class="">
									<?php if (get_sub_field('section_title')) { ?>
                                        <h2 class="section-title"><?php the_sub_field('section_title');?></h2>
                                    <?php } ?>
                                    <?php if (get_sub_field('section_content')) { ?>
                                        <?php the_sub_field ('section_content');?>
                                    <?php } ?>
                                    <?php if (get_sub_field('shortcode')) { ?>
                                        <?php echo do_shortcode(get_sub_field('shortcode')); ?>
                                    <?php } ?>
                              	</div>
                            <?php if ( $orgba ) { ?>
                                </div>
                            <?php } ?>
                        </div>
                <?php } ?>
            <!--</div>-->
      	</div>
    <?php if (get_sub_field( 'anchor' ) ) { ?>
        <a id="<?php the_sub_field( 'anchor' ); ?>"></a>
    <?php } ?>
        </div>