	<?php 
        $setColor =  get_sub_field('background_colour', $post->ID);
        $color = $setColor;
        $rgb = hex2rgba($color);
        $rgba = hex2rgba($color, 1);
        $border = get_sub_field('border_location');
    ?>
    <?php if (get_sub_field( 'anchor' ) ) { ?>
        <a id="<?php the_sub_field( 'anchor' ); ?>"></a>
    <?php } ?>
        <?php if ( $rgba ) { ?>
            <div class="gallery-slider <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;<?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>">
        <?php } else { ?>
            <div class="section gallery-slider <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="
            <?php if (get_sub_field('border')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>">
        <?php } ?>
        		<?php if (have_rows('gallery_images')) { ?>
                    <div class="flexslider">
                        <ul class="slides">
							<?php while(have_rows('gallery_images')) { ?>
                            	<?php the_row();?>
                                <?php $image = get_sub_field('slide_image'); ?>
                                <?php $size = 'full'; ?>
                                <?php if( $image ){ ?>
									<?php //print_r ($image) ; ?>
                                        <li class="image-slide">
											<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                            <?php if (get_sub_field('slide_link_text')) { ?>
                                            	<div class="content">
                                                    <div class="slide-overlay <?php the_sub_field('slide_link_text_position');?>">
                                                        <?php the_sub_field('slide_link_text');?>
                                                        <?php if(get_sub_field('slide_link')) { ?>
                                                            <a href="<?php the_sub_field('slide_link');?>">Find out more <i class="fas fa-chevron-right"></i></a>
                                                        <?php } ?>
                                                    </div>
                                             	</div>
                                            <?php } ?>
                                       	</li>
								<?php } ?>
							<?php } ?>
                        </ul>
                    </div>
                <?php } ?>
				</div>